<?php // Sidebar Blog
?>

<aside class="blog-sidebar">
	<div class="blog-sidebar__content">
		<?php if (is_active_sidebar('rcvf-sidebar-blog')) dynamic_sidebar('rcvf-sidebar-blog'); ?>
	</div>
</aside>
