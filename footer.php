<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">
	<div class="footer-widgets">
		<div class="container">
			<?php if (is_active_sidebar('rcvf-sidebar-footer-1')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('rcvf-sidebar-footer-1'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('rcvf-sidebar-footer-2')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('rcvf-sidebar-footer-2'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('rcvf-sidebar-footer-3')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('rcvf-sidebar-footer-3'); ?>
				</div>
			<?php endif; ?>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-widgets -->

	<div class="footer-copyright">
		<div class="container">
			<span>&copy;&nbsp;<?php echo bloginfo('title'); ?>&nbsp;<?php echo wp_date('Y') . '.'; ?>&nbsp;<?php _e('All rights reserved.', 'rcvf') ?></span>
			<span>
				<?php _e('Developed by', 'rcvf') ?>
				&nbsp;
				<a href="https://www.vollup.com/" target="_blank" rel="noopener">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/vollup.png'; ?>" alt="Vollup">
				</a>
			</span>

		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
