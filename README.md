# RCVF Advogados WordPress Custom Theme

<p align="center">
    <img alt="screenshot" title="Screenshot" src="./screenshot.png" />
</p>

This is a WordPress Custom theme made for the advocates office [RCVF Advogados](https://rcvf.com.br/)

## Getting started

Download the code from this repository and place it in a folder inside your WordPress installation themes folder, like this path:

```
\wp-content\themes\rcvf

```

Then you will see and be able to activate the theme on your WordPress dashboard > Appearance > Themes

## 🛠 Technologies
This project was developed with the following technologies

- [WordPress](https://br.wordpress.org/)
- [SASS](https://sass-lang.com/)
- [Node.js](https://nodejs.org/)
- [Webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)

## 👨‍💻 Editing the code

If you want to make editions to this projets code, like the CSS or Js, is recomended to have [Node.js](https://nodejs.org/) installed
and configured, so you can use it to compile [SASS](https://sass-lang.com/) into CSS and make the JavaScript well organized and light.

Open this code in your favorite editor, use the NPM or YARN packages managers to install all developments dependencies and run the scripts on the command line:

## 👉 For compile SASS:

```
npm run watch:sass

```
or
```
yarn watch:sass

```
## 👉 For compile JavaScript:

```
npm run watch:js

```
or
```
yarn watch:js

```

---

### ☕❤

Robson H. Rodrigues