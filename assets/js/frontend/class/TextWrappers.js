export default class TextWrappers {
	constructor(text) {
		this.text = text;
	}

	wrapChars() {
		return this.text.replace(/\w/g, "<span>$&</span>");
	}

	wrapWords() {
		return this.text.replace(/\w+/g, "<span>$&</span>");
	}

	wrapLines() {
		return this.text.replace(/.+$/gm, "<span>$&</span>");
	}
}
