export default class Navbar {
	constructor() {
		this.body = document.querySelector("body");
		this.wpadminbar = document.querySelector("#wpadminbar");
		this.navbar = document.querySelector("#navbar");
		this.viwewportX = document.documentElement.clientWidth;
		this.offset = this.navbar.offsetHeight;
		this.events();
	}

	// Events Triggers
	events() {
		if (this.navbar) {
			window.addEventListener("scroll", () => {
				this.handleStickEffect();
				this.handleWhenLoggedIn();
			});

			window.addEventListener("resize", () => {
				this.handleViwewportSize();
			});

			this.handleNavbarToggle();
		}
	}

	// Methods
	handleStickEffect() {
		if (window.scrollY > this.offset) {
			this.navbar.classList.add("sticky");
		} else {
			this.navbar.classList.remove("sticky");
		}
	}

	handleWhenLoggedIn() {
		// When the WP Admin Bar is visible, we need to change the top space to stick the navbar menu visible below it
		if (window.scrollY > this.offset) {
			if (this.wpadminbar && this.viwewportX > 576)
				this.navbar.style.top = this.wpadminbar.offsetHeight + "px";
		} else {
			if (this.wpadminbar && this.viwewportX > 576)
				this.navbar.style.top = "initial";
		}
	}

	handleViwewportSize() {
		this.viwewportX = document.documentElement.clientWidth;
	}

	handleNavbarToggle() {
		let navbarToggler = this.navbar.querySelector(".navbar-toggler");
		let contentOverlay = this.navbar.querySelector(".navbar-content-overlay");
		let icon = navbarToggler.querySelector(".navbar-toggler-icon");
		let target = navbarToggler.dataset.target;

		target = document.querySelector(target);

		if (target) {
			navbarToggler.addEventListener("click", () => {
				this.body.classList.toggle("no-scroll-vertical");

				navbarToggler.classList.toggle("show");
				contentOverlay.classList.toggle("show");
				target.classList.toggle("show");
				icon.classList.toggle("close");
			});
		}
	}
}
