import TextWrappers from "../class/TextWrappers";

export default class Navbar {
	constructor() {
		this.headerTitle = document.querySelector(".header-title");

		this.events();
	}

	// Events Triggers
	events() {
		if (this.headerTitle) {
			this.handleTitle();
		}
	}

	handleTitle() {
		const titleText = this.headerTitle.innerText;
		const wrappedTitleWords = new TextWrappers(titleText).wrapWords();

		this.headerTitle.innerHTML = wrappedTitleWords;
	}
}
