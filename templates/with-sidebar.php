<?php
/*
Template Name: With Sidebar
Template Post Type: post
*/

get_header();
?>

<main>
	<div class="container">
		<header class="entry-header">
			<?php
			the_title("<h1 class='entry-title'>", "</h1>");

			echo sprintf("<p class='post-date'>%s</p>", date_i18n(get_option('date_format'), get_post_timestamp()));
			?>
		</header>

		<section class="entry-content">
			<?php
			while (have_posts()) {
				the_post();

				the_post_thumbnail("full", array("title" => get_the_title()));

				get_template_part("partials/content/content", get_post_format());
			}

			the_post_navigation(
				array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __('Next Post', 'rcvf') . '</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __('Previous Post', 'rcvf') . '</span>',
				)
			);

			if (comments_open() || get_comments_number()) comments_template();

			get_template_part("partials/content/content", "related");
			?>
		</section>

		<?php get_sidebar(); ?>
	</div>
	<!-- /.container -->
</main>

<?php
get_footer();
