<?php

/**
 * Generic template part to display publication
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<!-- /.entry-content -->

	<footer class="entry-footer">
		<hr>
		<p>
			<?php _e('Posted in', 'rcvf'); ?>
			<?php the_date(); ?>
			<br>
			<?php _e('Categories:', 'rcvf'); ?>
			<?php the_category(', '); ?>
			<br>
			<?php if (has_tag()) : ?>
				<?php _e('Tags:', 'rcvf'); ?>
				<?php the_tags(' ', ' ', ' '); ?>
			<?php endif; ?>
		</p>
		<hr>
	</footer>
</article><!-- #post-<?php the_ID(); ?> -->
