<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('post-excerpt'); ?>>
	<div class="column-date">
		<p class="day"><?php echo date_i18n('D', get_post_timestamp()); ?></p>
		<p class="date"><?php echo date_i18n('d', get_post_timestamp()); ?></p>
	</div>
	<!-- /.column-date -->

	<div class="column-entry">
		<header class="entry-header">
			<p class="post-date"><?php echo date_i18n(get_option('date_format'), get_post_timestamp()); ?></p>

			<?php
			if (is_sticky() && is_home() && !is_paged()) {
				printf('<span class="sticky-post">%s</span>', _x('Post', 'Featured', 'rcvf'));
			}

			the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
			?>
		</header>
		<!-- /.entry-header -->

		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div>

		<footer class="entry-footer">
			<a class="read-more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('Read more', 'rcvf') ?></a>
		</footer>
		<!-- /.entry-footer -->
	</div>
	<!-- /.column-entry -->

	<div class="column-thumbnail">
		<a class="post-thumbnail" href="<?php echo esc_url(get_permalink()); ?>">
			<?php
			the_post_thumbnail('thumbnail', array('title' => get_the_title()));
			the_title('<span class="screen-reader-text">', '</span>');
			?>
		</a>
	</div>
	<!-- /.column-thumbnail -->

</div><!-- #post-<?php the_ID(); ?> -->
