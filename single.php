<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main>
	<?php
	while (have_posts()) {
		the_post();
		get_template_part('partials/content/content', get_post_format());
	}
	?>

	<div class="container">
		<?php
		the_post_navigation(
			array(
				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __('Next Post', 'rcvf') . '</span>',
				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __('Previous Post', 'rcvf') . '</span>',
			)
		);

		if (comments_open() || get_comments_number()) comments_template();

		get_template_part('partials/content/content', 'related');
		?>
	</div>
	<!-- /.container -->
</main>

<?php
get_footer();
