<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Front-End
function rcvf_scripts()
{
	// Web Fonts
	wp_enqueue_style('montserrat', '//fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap', null, '1.0', 'all');

	// CSS
	wp_enqueue_style('rcvf-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('rcvf-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('rcvf-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'rcvf_scripts');

// Admin Panel
function rcvf_admin_scripts()
{
	// CSS
	wp_enqueue_style('rcvf_admin', get_template_directory_uri() . '/assets/css/admin/admin-styles.css');
}
add_action('admin_enqueue_scripts', 'rcvf_admin_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function rcvf_gutenberg_scripts()
{
	wp_enqueue_style('montserrat', '//fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap', null, '1.0', 'all');
	wp_enqueue_style('rcvf-editor-styles', get_template_directory_uri() . '/assets/css/admin/editor-styles.css', array(), wp_get_theme()->get('Version'));
}
add_action('enqueue_block_editor_assets', 'rcvf_gutenberg_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function rcvf_setup()
{
	// Enabling translation support
	$textdomain = 'rcvf';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 50,
		'width'       => 115,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));

	// Custom Header
	add_theme_support('custom-header', array(
		'default-image'      => get_template_directory_uri() . '/assets/images/default-header.jpg',
		'default-text-color' => 'ffffff',
		'width'              => 1920,
		'height'             => 800,
		'flex-width'         => true,
		'flex-height'        => true,
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'rcvf'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/editor-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	// Creates the specific color palette
	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('White', 'rcvf'),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
		array(
			'name'  => __('Cultured', 'rcvf'),
			'slug'  => 'cultured',
			'color' => '#f1f1f1',
		),
		array(
			'name'  => __('Light Gray', 'rcvf'),
			'slug'  => 'light-gray',
			'color' => '#cdcdcc',
		),
		array(
			'name'  => __('Marigold', 'rcvf'),
			'slug'  => 'marigold',
			'color' => '#e3a224',
		),
		array(
			'name'  => __('Black', 'rcvf'),
			'slug'  => 'black',
			'color' => '#000000',
		),
	));

	// Custom font sizes.
	add_theme_support('editor-font-sizes', array(
		array(
			'name' => __('Small', 'rcvf'),
			'size' => 16,
			'slug' => 'small',
		),
		array(
			'name' => __('Normal', 'rcvf'),
			'size' => 18,
			'slug' => 'normal',
		),
		array(
			'name' => __('Medium', 'rcvf'),
			'size' => 24,
			'slug' => 'medium',
		),
		array(
			'name' => __('Big', 'rcvf'),
			'size' => 40,
			'slug' => 'big',
		),
		array(
			'name' => __('Huge', 'rcvf'),
			'size' => 50,
			'slug' => 'huge',
		),
	));

	/**
	 * Custom blocks styles.
	 *
	 * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
	 * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
	 */

	// Core Blocks
	register_block_style('core/buttons', array(
		'name' => 'grid-four-big-buttons',
		'label' => __('Grid 4 Big Buttons', 'rcvf'),
	));
	register_block_style('core/image', array(
		'name' => 'with-hover-effect',
		'label' => __('With Hover Effect', 'rcvf'),
	));
	register_block_style('core/column', array(
		'name' => 'with-end-divider',
		'label' => __('With End Divider', 'rcvf'),
	));

	// External blocks
	register_block_style('master/title-with-border-block', array(
		'name' => 'with-brand-decoration',
		'label' => __('With Brand Decoration', 'rcvf'),
	));
}
add_action('after_setup_theme', 'rcvf_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function rcvf_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'rcvf'),
		'id' => 'rcvf-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in Footer.', 'rcvf'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'rcvf'),
		'id' => 'rcvf-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in Footer.', 'rcvf'),
	)));

	// Footer #3
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #3', 'rcvf'),
		'id' => 'rcvf-sidebar-footer-3',
		'description' => __('The widgets in this area will be displayed in the third column in Footer.', 'rcvf'),
	)));

	// Barra Lateral Blog #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Sidebar Blog', 'rcvf'),
		'id' => 'rcvf-sidebar-blog',
		'description' => __('The widgets in this area will be displayed in the blog sidebar.', 'rcvf'),
	)));
}
add_action('widgets_init', 'rcvf_sidebars');

/**
 * Remove website field from comment form
 *
 */
function rcvf_website_remove($fields)
{
	if (isset($fields['url']))
		unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'rcvf_website_remove');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';
