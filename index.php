<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>

<main>
	<div class="container">
		<div class="posts-grid">
			<?php
			if (have_posts()) {
				while (have_posts()) {
					the_post();
					get_template_part('partials/content/content', 'excerpt');
				}
			} else {
				get_template_part('partials/content/content', 'none');
			}
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>
	<!--/.container-->
	<?php the_posts_pagination(); ?>
</main>

<?php
get_footer();
